provider "azurerm" {
  tenant_id       = "57c345bb-f270-4952-b685-3600a4e67234"
  subscription_id = "b59e6ae6-572e-492e-81d3-0c219aac77b5"
  client_id       = "56dc12ca-f6c4-497b-b892-eae2b717fe86"
  client_secret   = "jBx7Q~etKqTDkIAZvZEL_OGIvdV-E_aY-B-a4"
  features {}
}

resource "azurerm_container_registry" "acr" {
  name                     = "testacrar"
  resource_group_name      = azurerm_resource_group.petclinic.name
  location                 = azurerm_resource_group.petclinic.location
  sku                      = "Standard"
  admin_enabled            = false
}

resource "azurerm_resource_group" "petclinic" {
  name     = "petclinic-rg"
  location = "West Europe"
}

resource "azurerm_kubernetes_cluster" "petclinic-ctl" {
  name                = "petclinic-cluster"
  location            = azurerm_resource_group.petclinic.location
  resource_group_name = azurerm_resource_group.petclinic.name
  dns_prefix          = "exampleaks1"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Production"
  }
}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.petclinic-ctl.kube_config.0.client_certificate
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.petclinic-ctl.kube_config_raw

  sensitive = true
}
